﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private Transform interactorTransform;
    private Rigidbody2D rigidBody;
    private Vector2 movement;
    private Animator animator;
    private Vector3 defaultScale, flippedScale;

    private static Quaternion ROTATION_0 = Quaternion.Euler(0, 0, 0);
    private static Quaternion ROTATION_90 = Quaternion.Euler(0, 0, 90);
    private static Quaternion ROTATION_180 = Quaternion.Euler(0, 0, 180);
    private static Quaternion ROTATION_270 = Quaternion.Euler(0, 0, 270);


    // Start is called before the first frame update
    void Awake()
    {
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        defaultScale = new Vector3(-1, 1, 1);
        flippedScale = new Vector3(1, 1, 1);
    }
    void Start()
    {

    }

    // Update is called once per frame
    // framerate constantly changes so physics becomes unreliable
    // get input here
    void Update()
    {
        /////////////////////////////////////////
        // GET USER INPUT
        // Gives 0 for no change, -1 for left, +1 for right
        // works with arrow keys, wasd keys and controller input
        // check out new input system by Unity
        movement.x = Input.GetAxisRaw("Horizontal");
        // -1 for down, +1 for up
        movement.y = Input.GetAxisRaw("Vertical");

        ////////////////////////////////////////////
        // Update animation parameters
        // Set horizontal animation parameter to horizontal movement
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude); // length of movement vector, sqrmagnitude if movement
        // Also update last x,y direction values so we know where to face when idle
        if (movement.x != 0 || movement.y != 0)
        {
            animator.SetFloat("LastHorizontal", movement.x);
            animator.SetFloat("LastVertical", movement.y);
        }

        ////////////////////////////////////////////
        // Transform updates
        // We're using the sprite for the right and left direction
        // so if the input is to the left, flip the player sprite
        if (movement.x > 0 || animator.GetFloat("LastHorizontal") > 0)
        {
            transform.localScale = flippedScale;
        }
        else
        {
            transform.localScale = defaultScale;

        }

        if (movement.x > 0)
        {
            interactorTransform.localRotation = ROTATION_0;
        }

        if (movement.y > 0)
        {
            interactorTransform.localRotation = ROTATION_90;
        }
        else if (movement.y < 0)
        {
            interactorTransform.localRotation = ROTATION_270;
        }
    }

    // executed on a fixed time, by default 50x/sec
    // handle movement here
    void FixedUpdate()
    {
        rigidBody.MovePosition(rigidBody.position + movement * moveSpeed * Time.fixedDeltaTime);

    }
}
